# How to create annotations based Spring MVC application without web.xml #

Recently in work I needed to refresh knowledge about how to bootstrap spring mvc application without web.xml and how to configure it with annotations only. It's not a rocket science but let's take a look on how to do that...It always comes handy.

## Maven dependencies ##

```
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <groupId>spring-mvc-test</groupId>
  <artifactId>org.mymvc.test</artifactId>
  <version>0.0.1-SNAPSHOT</version>
  <packaging>war</packaging>
  <name>spring-mvc-test</name>
  <description>description test</description>
  
  <properties>
    <springframework.version>4.2.6.RELEASE</springframework.version>
  </properties>
  
  <dependencies>
      <dependency>
          <groupId>org.springframework</groupId>
          <artifactId>spring-webmvc</artifactId>
          <version>${springframework.version}</version>
      </dependency>
       
      <dependency>
          <groupId>javax.servlet</groupId>
          <artifactId>javax.servlet-api</artifactId>
          <version>3.1.0</version>
      </dependency>
      
  </dependencies>
  
  <build>
    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-war-plugin</artifactId>
        <configuration>
          <failOnMissingWebXml>false</failOnMissingWebXml>
        </configuration>
      </plugin>
      <plugin>
  		<groupId>org.apache.tomcat.maven</groupId>
  		<artifactId>tomcat7-maven-plugin</artifactId>
  		<version>2.2</version>
  		<configuration>
    		<path>/</path>
  		</configuration>
	  </plugin> 
    </plugins>
  </build>
    
</project>
```
Important is to configure maven-war-plugin to ignore missing web.xml file (**failOnMissingWebXml**), because we won't need it! Other things are usual.

## Old web.xml configuration ##

*To have your application REST Spring MVC ready you need to have **DispatcherServlet** configured*. DispatcherServlet is HTTP central dispatcher for handling of HTTP requests. Web.xml file in the old style could be:


```
<web-app id="WebApp_ID" version="2.4"
    xmlns="http://java.sun.com/xml/ns/j2ee" 
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://java.sun.com/xml/ns/j2ee 
    http://java.sun.com/xml/ns/j2ee/web-app_2_4.xsd">
 
    <display-name>Spring MVC Application</display-name>

   <servlet>
      <servlet-name>DispatcherServlet</servlet-name>
      <servlet-class>
         org.springframework.web.servlet.DispatcherServlet
      </servlet-class>
      <load-on-startup>1</load-on-startup>
   </servlet>

   <servlet-mapping>
      <servlet-name>DispatcherServlet</servlet-name>
      <url-pattern>/*</url-pattern>
   </servlet-mapping>

</web-app>
```

Well, we've been living with this style of Spring MVC applications boot-strapping quite a long time. By default DispatcherServlet expected to find [servlet-name]-servlet.xml Spring context in a project to bootstrap our Spring Beans. **Let's show the same configuration with Java code**:

```
public class MyWebInitializer implements WebApplicationInitializer {

	public void onStartup(ServletContext servletContext)
			throws ServletException {
        WebApplicationContext context = getContext();
        servletContext.addListener(new ContextLoaderListener(context));
        ServletRegistration.Dynamic dispatcher = servletContext.addServlet(
        		"DispatcherServlet", new DispatcherServlet(context));
        dispatcher.setLoadOnStartup(1);
        dispatcher.addMapping("/*");
	}
	
    private AnnotationConfigWebApplicationContext getContext() {
        AnnotationConfigWebApplicationContext context = 
        		new AnnotationConfigWebApplicationContext();
        context.register(ContextRootConfig.class);
        return context;
    }
}
```
**Focus on the getContext() method**, in this method we're saying that our Spring Application is going to be annotation based ("AnnotationConfigWebApplicationContext"). AnnotationConfigWebApplicationContext is basically equivalent to **AnnotationConfigApplicationContext** context which is on the other hand designated for standalone non-web applications. 

By calling:

```
context.register(ContextRootConfig.class);

```

we're registering following Spring configuration.

```
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebMvc
@ComponentScan(value = "org.mymvc.test")
public class ContextRootConfig {
}
```

which is equivalent to 

```
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
    xmlns:mvc="http://www.springframework.org/schema/mvc"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="
        http://www.springframework.org/schema/beans 
        http://www.springframework.org/schema/beans/spring-beans-3.0.xsd
        http://www.springframework.org/schema/mvc
        http://www.springframework.org/schema/mvc/spring-mvc-3.0.xsd">

    <mvc:annotation-driven/>
	<context:component-scan base-package="org.mymvc.test" />

</beans>
```

Pretty easy, isn't it? Let's add simple Spring MVC controller to see that our annotations based application is running ok.


```
@RestController
public class SpringMVCController {
	
	@RequestMapping(path = "/greeting/{personId}", method=RequestMethod.GET)
	public String greetings(@PathVariable("personId") String personId) {
		return "Hello from Spring 4: "+personId;
	}
}
```
**@RestController** annotation = Controller annotation plus it adds ResponseBody annotation to all methods. This controller will just simply answer to all GET requests on the address <domain>:8080/greeting/{personId} with:

```
"Hello from Spring 4: "+{personId}
```

## How to run the demo ##

* git clone this repo
* mvn clean install (in the maven root directory with pom.xml)
* mvn tomcat7:run-war

now with curl you can hit:

```
curl -v http://localhost:8080/greeting/Tomas
*   Trying ::1...
* Connected to localhost (::1) port 8080 (#0)
> GET /greeting/Tomas HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/7.43.0
> Accept: */*
> 
< HTTP/1.1 200 OK
< Server: Apache-Coyote/1.1
< Content-Type: text/plain;charset=ISO-8859-1
< Content-Length: 26
< Date: Sun, 05 Jun 2016 10:30:57 GMT
< 
* Connection #0 to host localhost left intact
Hello from Spring 4: Tomas

```

Hope you found this useful. See you next time

regards

Tomas